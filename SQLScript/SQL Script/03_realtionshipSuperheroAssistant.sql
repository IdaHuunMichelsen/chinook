USE SuperheroesDB
ALTER TABLE Assistant ADD Superhero_Id int;
ALTER TABLE Assistant
ADD CONSTRAINT FKSuperhero_Id FOREIGN KEY (Superhero_Id)
REFERENCES Superhero(Superhero_Id);