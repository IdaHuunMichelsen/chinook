USE SuperheroesDB



CREATE TABLE Superhero(
Superhero_Id int IDENTITY (1,1) PRIMARY KEY,
NAME Nvarchar(50),
Alias Nvarchar(50),
Origin Nvarchar(50)
);

CREATE TABLE Assistant(
Assistant_Id int IDENTITY (1,1) PRIMARY KEY,
NAME Nvarchar(50)

);

CREATE TABLE Power(
Power_Id int IDENTITY (1,1) PRIMARY KEY,
NAME Nvarchar(50),
DESCRIPTION Nvarchar(50)

);