**About the project**

This project was created as an assignment consisting of two parts.

Appendix A: SQL scripts to create a database.

Appendix B: Reading data with SQL Client from the Chinook database.
1. A function that shows you all the customers in the database.
2. A function that shows you one specific customer from the database by ID.
3. A function shows you one specific customer from the database by Name.
4. A function shows you a subset of customers from the database by limit and offset.
5. A function adds as new customer to the databse.
6. A function updates an existing customer in the database.
7. A function returns the number of customers in each country ordered by descending.
8. A function shows the customer with the highest spendings ordered by descending.
9. A function shows you one specific customer from the database with his/her most popular genre(s).

**Technologies**

- Visual Studio
- Microsoft SQL Server Management Studio





**Contributors**

Kjetil Rønhovde & Ida Huun Michelsen
