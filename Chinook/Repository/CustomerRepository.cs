﻿using Chinook.Model;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        // Creating a list to get all Customers from the database
        public List<Customer> GetAllCustomers()
        {

            List<Customer> customerList = new List<Customer>();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {

                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                //Assign value from database to the ID
                                temp.ID = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);

                                try
                                {
                                    temp.PostalCode = reader.GetString(4);
                                }
                                catch (Exception ex)
                                {

                                    temp.PostalCode = null;
                                }
                                ;
                                try
                                {
                                    temp.PhoneNumber = reader.GetString(5);
                                }
                                catch (Exception ex)
                                {

                                    temp.PhoneNumber = null;
                                }
                                temp.Email = reader.GetString(6);
                                //Add to the collection List.
                                customerList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        // Get a spesific customer by id
        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();


            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer where CustomerId = @ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))

                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.PhoneNumber = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        // Get a spesific customer by first name
        public Customer GetCustomer(string FirstName)
        {
            Customer customer = new Customer();


            string sql = "select CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email From Customer where FirstName Like @FirstName";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))

                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", FirstName);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.PhoneNumber = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }


        //To get a subset of the customer list
        public List<Customer> GetSubsetCustomers(int limit, int offset)
        {
            List<Customer> customerToReturn = new List<Customer>();



           
            List<Customer> customerList = new List<Customer>();


            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))

                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@offset", offset);
                        cmd.Parameters.AddWithValue("@limit", limit);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                //Assign value from database to the id
                                temp.ID = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);

                                try
                                {
                                    temp.PostalCode = reader.GetString(4);
                                }
                                catch (Exception ex)
                                {

                                    temp.PostalCode = null;
                                }
                                ;
                                try
                                {
                                    temp.PhoneNumber = reader.GetString(5);
                                }
                                catch (Exception ex)
                                {

                                    temp.PhoneNumber = null;
                                }
                                temp.Email = reader.GetString(6);

                                customerList.Add(temp);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }

        // Method for adding a new customer
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "Insert into Customer( FirstName, LastName, Country, PostalCode, Phone, Email) Values(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {

                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        //Whenever there is a change in Database - ExecuteNonQuery()
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }

        // Method for updating info about a customer
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer set  FirstName=@FirstName, LastName=@LastName,Country=@Country, PostalCode=@PostalCode, Phone=@Phone, Email=@Email  where CustomerId=@CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.ID);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        //Whenever there is a change in Database - ExecuteNonQuery()
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
            }
            return success;
        }

        // Method for returning number of customers from each country in descending order
        public List<CustomerCountry> GetCustomerCountry()
        {
            List<CustomerCountry> customerNumbers = new List<CustomerCountry>();
            string query = "SELECT Country, COUNT(CustomerId) AS total FROM Customer GROUP BY Country ORDER BY total DESC;";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Creating a new CustomerCountry object that contais the name of the country and the amount of customers
                            CustomerCountry customerCountryNumber = new CustomerCountry();
                            customerCountryNumber.Country = reader.GetString(0);
                            customerCountryNumber.Members = reader.GetInt32(1);

                            // Addng the CustomerCountry object to the list
                            customerNumbers.Add(customerCountryNumber);
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return customerNumbers;
        }

        public CustomerSpender GetHighestSpender()
        {
            // Method for highest spending costumer in descending order
            CustomerSpender customerSpender = new CustomerSpender();

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    string query = @"SELECT Customer.CustomerId, SUM(Invoice.Total) AS total FROM Customer, Invoice WHERE Customer.CustomerId = Invoice.CustomerId GROUP BY Customer.CustomerId ORDER BY total DESC";

                    using (SqlCommand command = new SqlCommand(query, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            try
                            {
                                while (reader.Read())
                                {
                                    // Adding CustomerID and the total invoice amount to the CustomerSpender dictionary
                                    customerSpender.AddCustomerSpendings(reader.GetInt32(0), reader.GetDecimal(1));
                                }
                                reader.Close();
                            }
                            catch (SqlException ex)
                            {
                                Console.WriteLine(ex);
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerSpender;
                 
        }

        // Method for finding most populare genre
        public List<CustomerGenre> GetPopularGenre(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
            {
                connection.Open();
                Console.WriteLine("Done. \n");

                List<CustomerGenre> custList = new List<CustomerGenre>();
                string sql = "WITH GenrePopularity AS" +
                    "(SELECT COUNT(Genre.Name) AS Popularity, Customer.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name " +
                    "FROM ((((Customer " +
                    "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId) " +
                    "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId) " +
                    "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId) " +
                    "INNER JOIN Genre ON Track.GenreId = Genre.GenreId) WHERE Customer.CustomerId = @CustomerId GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name) " +
                    "SELECT gp.CustomerId, gp.FirstName, gp.LastName, gp.Name, gp.Popularity " +
                    "FROM GenrePopularity gp " +
                    "WHERE gp.Popularity = (SELECT max(Popularity) FROM GenrePopularity)";
                try
                {
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        command.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.LastName = reader.IsDBNull(2) ? null : reader.GetString(2);
                                temp.FavoriteGenre = reader.IsDBNull(3) ? null : reader.GetString(3);
                                temp.GenreCount = reader.GetInt32(4);
                                custList.Add(temp);
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex);
                }
                return custList;
            }
        }
    }
}
           
        
    
        





    
    




            
            
            

            


            
 
    


































