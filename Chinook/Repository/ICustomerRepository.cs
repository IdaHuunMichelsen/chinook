﻿using Chinook.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Repository
{
    public interface ICustomerRepository
    {
        // To get all Customers
        public List<Customer> GetAllCustomers();

        // To get a single customer by id
        public Customer GetCustomer(int id);

        // To get a single customer by name
        public Customer GetCustomer(string name);

        // To get subset of customer list
        public List<Customer> GetSubsetCustomers(int offset, int limit);

        // To add a new customer
        public bool AddNewCustomer(Customer customer);

        // To update the info about a customer
        public bool UpdateCustomer(Customer customer);

        // To see number of customers in each country
        public List <CustomerCountry> GetCustomerCountry();

        // To see the highest spendig customers
        public CustomerSpender GetHighestSpender();

        //To see the most populare genre
        public List<CustomerGenre> GetPopularGenre(int id);


    }
}
