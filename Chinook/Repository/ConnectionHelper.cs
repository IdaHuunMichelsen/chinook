﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Repository
{
    public class ConnectionHelper
    {

        // ConnectionHelper to help manage the database connections
        
        public static string GetConnectionString()
        {
            
            SqlConnectionStringBuilder connectStringBuilder = new SqlConnectionStringBuilder();
            connectStringBuilder.DataSource = "DESKTOP-OC99RQJ\\SQLEXPRESS";
            connectStringBuilder.InitialCatalog = "Chinook";
            connectStringBuilder.IntegratedSecurity = true;
            return connectStringBuilder.ConnectionString;
        }
    }
}
