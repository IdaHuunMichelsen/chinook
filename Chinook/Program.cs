﻿using Chinook.Model;
using Chinook.Repository;
using System.Net.WebSockets;
using System.Runtime.InteropServices;

namespace Chinook
{
     class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            //PrintAllCustomers(repository);
            //PrintOneCustomer(repository);
            //InsertCustomer(repository);
            //UpdateCustomer(repository);
            //SubsetCustomers(repository);
            
            
            //(7) Number of customers in each country
            List<CustomerCountry> countryCount = repository.GetCustomerCountry();

            foreach (var entry in countryCount)
            {
                Console.WriteLine(entry.Country+": "+ entry.Members);
            }

            //(8) Highest spending customer
            {
                Console.WriteLine(repository.GetHighestSpender());
            }
      
            Console.ReadKey();          
        }


        //(1) Read all costumers and display their: id, first name, last name, country, postal code, phone number and email
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        private static void PrintAllCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAllCustomers());
        }


        //(2 & 3) Read one customer by their id and name
        static void PrintOneCustomer(ICustomerRepository repository)
        {
           //Input for printing a Customer by Id

            PrintCustomer(repository.GetCustomer(2));
            PrintCustomer(repository.GetCustomer("Robert"));            
        }


        static void PrintCustomer(Customer customer) 
            {
            
                    Console.WriteLine($"{customer.ID}, {customer.FirstName}, {customer.LastName}, {customer.Country},{customer.PostalCode}, {customer.PhoneNumber},{customer.Email} ");
                    
            }

        //(5) Adding a new customer
        static void InsertCustomer(ICustomerRepository repository)
        {
            Customer newCustomer = new Customer()
            {
                
                FirstName = "Donald",
                LastName = "Smith",
                Country = "Peru",
                PostalCode= "0102",
                PhoneNumber = "99999999",
                Email = "donald.smith@mail.com"
            };
            if (repository.AddNewCustomer(newCustomer))
            {
                Console.WriteLine("New customer added!");
                PrintCustomer(repository.GetCustomer(100));
            }
            else
            {
                Console.WriteLine("Problem with adding customer.");
            }
        }

        //(6) Update info about a customer
        static void UpdateCustomer(ICustomerRepository repository)
        {
            Customer update = new Customer()
            {
                ID = 1,
                FirstName = "first name",
                LastName = "Last name",
                Country = "Norway",
                PostalCode = "0000",
                PhoneNumber = "00000000",
                Email = "not@mail.com"
            };
            if (repository.UpdateCustomer(update))
            {
                Console.WriteLine("Successfully updated record");
                PrintCustomer(repository.GetCustomer(1));
            }
            else
            {
                Console.WriteLine("not successful");
            }
        }

        //(4) Subset page of customers
        static void SubsetCustomers(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetSubsetCustomers(5, 35));
        }

        //(9) For a gicen customer, their most populare genre
        public void PrintCustomerFavoriteGenre(IEnumerable<CustomerGenre> customers)
        {
            foreach (CustomerGenre customer in customers)
            {
                Console.WriteLine($" (customer.CustomerId)(customer.FirstName)(customer.LastName)(customer.FavoriteGenre)(customer.GenreCpunt)"); 
            }
        }
        public void SelectFavoriteGenre(ICustomerRepository repository)
        {
            PrintCustomerFavoriteGenre(repository.GetPopularGenre(15));
        }



    }
    
}