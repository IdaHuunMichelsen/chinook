﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Model
{
    //Dictionary for add customer spendings
    public class CustomerSpender
    {
        public Dictionary<int, decimal> CustomerSpendings = new Dictionary<int, decimal>();
        public void AddCustomerSpendings(int customerId, decimal spendings)
        {
            CustomerSpendings.Add(customerId, spendings);
        }


        //Creating string builder
        public override string ToString()
        {
            StringBuilder returnString = new StringBuilder();

            foreach (KeyValuePair<int, decimal> customer in CustomerSpendings)
            {
                returnString.Append("Customer Id: " + customer.Key + " Spending: " + customer.Value + "\n\n");
            }
            return returnString.ToString();
        }

    }
}
