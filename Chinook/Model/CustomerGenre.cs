﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Model
{
    // Model for the data structure used when finding what genre a customer has as its most popular
    
        public class CustomerGenre
        {
            public int CustomerId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string FavoriteGenre { get; set; }
            public int GenreCount { get; set; }
        }
    }


