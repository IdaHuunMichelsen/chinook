﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chinook.Model
{
    public class CustomerCountry
    {
        //Properties for country
        public string Country { get; set; }
        public int Members { get; set; }
    }
}
